clear;
delete(instrfind);
opengl('save','software');
close all;
instrreset;

V_p =  0.5;
V_n = 0.1;
N = 500;
dV = (V_p-V_n)/N;

hold_t = 0.5;

voltage_Si = zeros(1,N+2);
current_Si = zeros(1,N+2);

paSi = gpib('ni',0,22);

fopen(paSi);

fprintf(paSi,'*RST');
pause(hold_t);
fprintf(paSi,"FUNC 'CURR'");
pause(hold_t);
fprintf(paSi,'SYST:ZCH ON');
pause(hold_t);
fprintf(paSi,'INIT');
pause(hold_t);
fprintf(paSi,'SYST:ZCOR:STAT OFF');
pause(hold_t);
fprintf(paSi,'SYST:ZCOR:ACQ');
pause(hold_t);
fprintf(paSi,'SYST:ZCOR ON');
pause(hold_t);
fprintf(paSi,'SYST:ZCH OFF');
pause(hold_t);
fprintf(paSi,'SOUR:VOLT:STAT ON');
pause(hold_t);
fprintf(paSi,'CURR: RANG 20M');
pause(hold_t);

hold on

j=1;


for v=V_n+dV:dV:V_p

    fprintf(paSi,'SOUR:VOLT %d',v);
    fprintf(paSi,'READ?');
    pause(hold_t);
    
    voltage_Si(j) = v;
    reading_Si = fscanf(paSi);
    
    current_Si(j) = str2double(reading_Si(1:13));
    if current_Si(j) > 1
        current_Si(j) = -.25;
    end
    
    scatter(voltage_Si(j),current_Si(j),'r');
    j = j+1;
end

fprintf(paSi,'SOUR[1]:VOLT:STAT OFF');

fclose(paSi);

fileID = fopen('3_21_23Ge_20.txt','w');
fprintf(fileID,'%14s %14s\r\n','current','voltage');
fprintf(fileID,'%14.10f %14.10f\r\n',[current_Si; voltage_Si]);
fclose(fileID);



